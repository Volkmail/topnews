import React from 'react';
import { Container } from '@mui/material';

import Header from '@components/Header';
import NewsList from '@pages/NewsList';
import { Route, Routes } from 'react-router-dom';

const App: React.FC<Record<string, unknown>> = () => (
  <Container
    disableGutters
    maxWidth={false}
    sx={{
      bgcolor: 'background.default',
      height: '100vh',
      margin: 0,
      padding: 0,
      display: 'flex',
      flexDirection: 'column',
    }}>
    <Header />
    <Routes>
      <Route path='/' element={<NewsList />} />
      <Route path='news/:id' element={<div>SomeNew</div>} />
      <Route path='settings' element={<div>Settings Page</div>} />
      <Route path='aboutUs' element={<div>AboutUs Page</div>} />
      <Route path='help' element={<div>Help Page</div>} />
      <Route path='*' element={<div>NoMatch</div>} />
    </Routes>
  </Container>
);

export default App;
