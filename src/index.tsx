import React from 'react';
import ReactDOM from 'react-dom/client';
import App from '@src/App';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { Provider } from 'react-redux';
import createTheme from '@src/theme';
import { store } from '@redux/store';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <BrowserRouter>
    <Provider store={store}>
      <ThemeProvider theme={createTheme()}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </Provider>
  </BrowserRouter>,
);
