import { AnyAction, applyMiddleware, combineReducers, createStore } from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { userSettingsReducer } from '@redux/reducers/userSettings';
import { newsListReducer } from '@redux/reducers/newsList';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

const rootReducer = combineReducers({
  userSettings: userSettingsReducer,
  newsList: newsListReducer,
});

type AppStateType = ReturnType<typeof rootReducer>;

const store = createStore(
  rootReducer,
  applyMiddleware<ThunkDispatch<AppStateType, undefined, AnyAction>, AppStateType>(thunk),
);

type AppDispatch = typeof store.dispatch;

const useAppDispatch = () => useDispatch<AppDispatch>();
const useAppSelector: TypedUseSelectorHook<AppStateType> = useSelector;

export { store, useAppDispatch, useAppSelector };

export type { AppStateType };
