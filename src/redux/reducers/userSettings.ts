import { UserSettingsActionsTypes } from '@actions/types';
import { SWITCH_THEME_MODE } from '@actions/userSettings';
import { ThemeMode } from '@src/theme';

type UserSettingsInitialStateType = {
  themeMode: ThemeMode;
};

const initialState: UserSettingsInitialStateType = {
  themeMode: ThemeMode.LIGHT,
};

const userSettingsReducer = (
  state: UserSettingsInitialStateType = initialState,
  action: UserSettingsActionsTypes,
): UserSettingsInitialStateType => {
  switch (action.type) {
    case SWITCH_THEME_MODE: {
      return {
        ...state,
        themeMode: state.themeMode === ThemeMode.LIGHT ? ThemeMode.DARK : ThemeMode.LIGHT,
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

export { userSettingsReducer };
