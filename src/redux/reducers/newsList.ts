import { NewsListActionsType } from '@actions/types';
import { Article } from '@redux/types';
import { GET_NEWS } from '@actions/newsList';

const initialState = {
  articles: [] as Article[],
  page: 1,
  pageSize: 10,
  totalResults: 0,
};

type NewsListInitialStateType = typeof initialState;

const newsListReducer = (
  state: NewsListInitialStateType = initialState,
  action: NewsListActionsType,
): NewsListInitialStateType => {
  switch (action.type) {
    case GET_NEWS: {
      return {
        ...state,
        totalResults: action.payload.totalResults,
        articles: [...action.payload.articles],
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

export { newsListReducer };
