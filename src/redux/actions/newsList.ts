import { NewsResponse } from '@api/types';

const GET_NEWS = 'news_list/LOAD_NEWS';

const getNews = (newsResponse: NewsResponse) =>
  ({
    type: GET_NEWS,
    payload: {
      totalResults: newsResponse.totalResults,
      articles: newsResponse.articles,
    },
  } as const);

export { getNews, GET_NEWS };
