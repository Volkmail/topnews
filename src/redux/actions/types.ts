import * as userSettingsActions from '@actions/userSettings';
import * as newsListActions from '@actions/newsList';

type ActionCreatorSelector<T> = T extends string ? never : T;
type InferValueTypes<T> = T extends { [key: string]: infer U } ? ActionCreatorSelector<U> : never;

type UserSettingsActionsTypes = ReturnType<InferValueTypes<typeof userSettingsActions>>;
type NewsListActionsType = ReturnType<InferValueTypes<typeof newsListActions>>;

export type { UserSettingsActionsTypes, NewsListActionsType };
