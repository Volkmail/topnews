const SWITCH_THEME_MODE = 'userSettings/SWITCH_THEME_MODE';

const switchThemeMode = () =>
  ({
    type: SWITCH_THEME_MODE,
  } as const);

export { switchThemeMode, SWITCH_THEME_MODE };
