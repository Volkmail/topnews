import { Dispatch } from 'react';
import { ThunkAction } from 'redux-thunk';
import { AppStateType } from '@redux/store';
import { NewsListActionsType } from '@actions/types';
import { NewsApi } from '@api/news';
import { getNews } from '@actions/newsList';

const loadNews =
  (): ThunkAction<void, AppStateType, unknown, NewsListActionsType> =>
  async (dispatch: Dispatch<NewsListActionsType>, getState) => {
    const state = getState();
    const response = await NewsApi.getNews(state.newsList.page, state.newsList.pageSize);
    if (response) {
      dispatch(getNews(response));
    }
  };

export { loadNews };
