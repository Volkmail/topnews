import { AppStateType } from '@redux/store';

const getThemeMode = (state: AppStateType): string => state.userSettings.themeMode;

export { getThemeMode };
