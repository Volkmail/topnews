import { AppStateType } from '@redux/store';
import { Article } from '@redux/types';

const getArticleList = (state: AppStateType): Article[] => state.newsList.articles;

// const getArticleList = createSelector(
//   (state: AppStateType): Article[] => state.newsList.articles,
//   (articles) => articles,
// );

export { getArticleList };
