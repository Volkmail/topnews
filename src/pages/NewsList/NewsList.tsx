import React, { useEffect } from 'react';
import { Container } from '@mui/material';
import { loadNews } from '@thunks/news';
import { useAppDispatch, useAppSelector } from '@redux/store';
import { getArticleList } from '@selectors/news';

const NewsList: React.FC<Record<string, unknown>> = () => {
  const dispatch = useAppDispatch();
  const articleList = useAppSelector(getArticleList);

  useEffect(() => {
    dispatch(loadNews());
  }, []);

  return (
    <Container>{articleList && articleList.map((article) => <div>{article.title}</div>)}</Container>
  );
};

export default NewsList;
