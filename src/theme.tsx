import { createTheme, Theme } from '@mui/material/styles';

// eslint-disable-next-line no-shadow
enum ThemeMode {
  LIGHT = 'light',
  DARK = 'dark',
}

export default (mode: ThemeMode = ThemeMode.LIGHT): Theme =>
  createTheme({
    palette: {
      mode,
      primary: {
        main: '#AB1500',
        light: '#ffffff',
        dark: '#000000',
      },
      text: {
        primary: '#000000',
        secondary: '#F5F5F5F5',
      },
      ...(mode === 'light'
        ? {
            background: {
              default: '#FFFFFF',
            },
          }
        : {
            background: {
              default: '#303030',
            },
          }),
    },
    typography: {
      h1: {
        fontFamily: 'Bebas Neue',
        fontSize: 50,
      },
      h2: {
        fontFamily: 'Bebas Neue',
        fontSize: 20,
      },
      h3: {
        fontFamily: 'Roboto Condensed',
        fontSize: 20,
      },
      subtitle1: {
        fontFamily: 'Roboto Condensed',
        fontSize: 18,
      },
      body1: {
        fontFamily: 'Roboto Condensed',
        fontSize: 16,
      },
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: `
        @font-face {
          font-family: 'Bebas Neue';       
          src: local("BebasNeue-Regular"), url("fonts/BebasNeue-Regular.ttf") format("truetype");
          font-weight: 400;                 
          font-style: normal;
        }
        
        @font-face {
          font-family: 'Roboto Condensed';       
          src: local("RobotoCondensed-Regular"), url("fonts/RobotoCondensed-Regular.ttf") format("truetype");
          font-weight: 400;                 
          font-style: normal;
        }
        
        @font-face {
          font-family: 'Roboto Condensed';       
          src: local("RobotoCondensed-Bold"), url("fonts/RobotoCondensed-Bold.ttf") format("truetype");
          font-weight: 700;                 
          font-style: normal;
        }
      `,
      },
    },
  });

export { ThemeMode };
