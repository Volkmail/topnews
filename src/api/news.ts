import axios from 'axios';
import { NewsResponse } from '@api/types';

const NewsApi = {
  getNews(page: number, pageSize: number) {
    return axios
      .get<NewsResponse>(
        `https://newsapi.org/v2/top-headlines?language=ru&page=${page}&pageSize=${pageSize}&apiKey=a38b38254f7848bca242e1cba060e5f0`,
      )
      .then((response) => response.data);
  },
};

export { NewsApi };
