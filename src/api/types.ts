import { Article } from '@redux/types';

type NewsResponse = {
  status: string;
  totalResults: number;
  articles: Article[];
};

export type { NewsResponse };
