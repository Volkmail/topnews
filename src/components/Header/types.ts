type HeaderPropsType = Record<string, unknown>;

export type { HeaderPropsType };
