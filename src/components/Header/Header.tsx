import React from 'react';
import { HeaderPropsType } from 'src/components/Header/types';
import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import MenuIcon from '@mui/icons-material/Menu';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import { LogoIcon } from '@components/Icons';
import { NavLink } from 'react-router-dom';

const Header: React.FC<HeaderPropsType> = () => {
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position='static' color='primary'>
      <Container maxWidth='xl'>
        <Toolbar
          disableGutters
          sx={[
            { display: 'flex', justifyContent: 'space-between' },
            {
              '&  a': {
                textDecoration: 'none',
                color: 'text.secondary',
              },
            },
          ]}>
          <Box>
            <NavLink to='/'>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <LogoIcon fontSize='large' />
                <Typography variant='h2' noWrap sx={{ marginLeft: '10px' }}>
                  TopNews
                </Typography>
              </Box>
            </NavLink>
          </Box>
          <Box sx={[{ display: { xs: 'none', sm: 'flex' } }]}>
            <Button onClick={handleCloseNavMenu}>
              <NavLink to='/settings'>Настройки</NavLink>
            </Button>
            <Button onClick={handleCloseNavMenu}>
              <NavLink to='/help'>Помощь</NavLink>
            </Button>
            <Button onClick={handleCloseNavMenu}>
              <NavLink to='/aboutUs'>О нас</NavLink>
            </Button>
          </Box>
          <Box sx={{ display: { xs: 'flex', sm: 'none' } }}>
            <IconButton size='large' onClick={handleOpenNavMenu} color='inherit'>
              <MenuIcon />
            </IconButton>
            <Menu
              anchorEl={anchorElNav}
              keepMounted
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                '&  a': {
                  textDecoration: 'none',
                  color: 'text.primary',
                },
              }}>
              <MenuItem onClick={handleCloseNavMenu}>
                <NavLink to='/settings'>
                  <Typography variant='body1' textAlign='center'>
                    Настройки
                  </Typography>
                </NavLink>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu}>
                <NavLink to='/help'>
                  <Typography variant='body1' textAlign='center'>
                    Помощь
                  </Typography>
                </NavLink>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu}>
                <NavLink to='/aboutUs'>
                  <Typography variant='body1' textAlign='center'>
                    О нас
                  </Typography>
                </NavLink>
              </MenuItem>
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;
