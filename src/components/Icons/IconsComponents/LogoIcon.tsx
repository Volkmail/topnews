import React from 'react';
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';
import LogoSvg from '@src/assets/logo.svg';

const LogoIcon: React.FC<SvgIconProps> = (props) => (
  <SvgIcon {...props}>
    <LogoSvg />
  </SvgIcon>
);

export { LogoIcon };
